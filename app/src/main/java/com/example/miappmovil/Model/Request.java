package com.example.miappmovil.Model;

import java.util.List;

public class Request {

    private String idUsuario;
    private String correo;
    private String phone;
    private String name;
    private String address;
    private String total;
    private String status;
    private List<Order> foods;

    public Request(String telefono, String nombre, String s, String toString, List<Order> cart) {
    }

    public Request(String idUsuario, String correo, String phone, String name, String address, String total, String status, List<Order> foods) {
        this.idUsuario = idUsuario;
        this.correo = correo;
        this.phone = phone;
        this.name = name;
        this.address = address;
        this.total = total;
        this.status = "0"; //defaul is 0 , 0 placed , 1 shiped , 2 shiped
        this.foods = foods;
    }


    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Order> getFoods() {
        return foods;
    }

    public void setFoods(List<Order> foods) {
        this.foods = foods;
    }
}
