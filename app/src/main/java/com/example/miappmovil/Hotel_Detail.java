package com.example.miappmovil;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.example.miappmovil.Common.Common;
import com.example.miappmovil.Database.Database;
import com.example.miappmovil.Model.Hotels;
import com.example.miappmovil.Model.Order;
import com.example.miappmovil.Model.User;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.Stack;

public class Hotel_Detail extends AppCompatActivity {
    TextView txtfood_name,txtfood_price,txtfood_description;
    ImageView img_food;
    CollapsingToolbarLayout collapsingToolbarLayout;
    Button siete, dies, doce, tres, btncard;
    ElegantNumberButton numberButton;
    String foodId="";
    FirebaseDatabase database;
    DatabaseReference foods;

    private Stack<Integer> num = new Stack<>();
    private int contador = 0;

    Hotels currentFood;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_detail);

        setupActionBar();

        //firebase
        database=FirebaseDatabase.getInstance();
        foods=database.getReference( "Foods");
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference table_user = database.getReference("User");

        //init view
        siete = findViewById(R.id.siete);
        dies = findViewById(R.id.dies);
        doce = findViewById(R.id.doce);
        tres = findViewById(R.id.tres);
        txtfood_name=findViewById(R.id.food_name);

        txtfood_price=findViewById(R.id.food_price);
        txtfood_description=findViewById(R.id.food_description);
        img_food=findViewById(R.id.img_food);
        collapsingToolbarLayout =findViewById(R.id.collapseActionView);
//        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppbar);

        btncard =findViewById(R.id.btncard);
        btncard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String txtname = txtfood_name.getText().toString();
                String txtapellido = txtfood_price.getText().toString();
                final String txtdireccion = txtfood_description.getText().toString();


                if (TextUtils.isEmpty(txtname) || TextUtils.isEmpty(txtapellido) || TextUtils.isEmpty(txtdireccion)) {
                    Toast.makeText(Hotel_Detail.this, "Ningun campo debe estar vacio", Toast.LENGTH_SHORT).show();

                }else {

                    if (Common.isConnectedToInternet(getBaseContext())) {
                        final ProgressDialog mDialog = new ProgressDialog(Hotel_Detail.this);
                        mDialog.setMessage("Por favor espere ......");
                        mDialog.show();
                        foods.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (dataSnapshot.child(siete.getText().toString()).exists()) {
                                    mDialog.dismiss();
                                    Toast.makeText(Hotel_Detail.this, "El horario que seleccionastes ya está reservado!", Toast.LENGTH_SHORT).show();

                                } else {
                                    mDialog.dismiss();
                                    User user = new User();


                                    foods.child(txtfood_name.getText().toString()).setValue(user);
                                    Toast.makeText(Hotel_Detail.this, "¡Regístrate éxitosamente tu reservacion!", Toast.LENGTH_SHORT).show();
                                    finish();
                                }

                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    } else {
                        Toast.makeText(Hotel_Detail.this, "Comprueba tu conexión a internet !", Toast.LENGTH_SHORT).show();

                    }
                }

            }

        });


        //get food id from intent
        if (getIntent()!=null)
            foodId=getIntent().getStringExtra("FoodId");
        if (!foodId.isEmpty())
        {//lll
            getDetailFood(foodId);
        }
    }




    //accion replicar
    public void btnAction(View view) {
        Toast.makeText(Hotel_Detail.this,"Selecciono primer turno",Toast.LENGTH_LONG).show();


        if (num.size() == 3){
            siete.setVisibility(View.GONE);

        }else {
            contador = contador + 1;
            num.push(contador);
        }
        Toast toast1 =
                Toast.makeText(getApplicationContext(),
                        "Ya no abran cupos disponibles en este horario", Toast.LENGTH_SHORT);

        toast1.show();
    }


    public void btnAction1(View view) {
        Toast.makeText(Hotel_Detail.this,"Selecciono Segundo turno",Toast.LENGTH_LONG).show();

        if (num.size() == 0){
            dies.setVisibility(View.GONE);

        }else {

            contador = contador + 1;
            num.push(contador);
        }
        Toast toast1 =
                Toast.makeText(getApplicationContext(),
                        "Ya no abran cupos disponibles en este horario", Toast.LENGTH_SHORT);

        toast1.show();
    }


    public void btnAction2(View view) {
        Toast.makeText(Hotel_Detail.this,"Selecciono Tercer turno",Toast.LENGTH_LONG).show();
        if (num.size() == 3){
            doce.setVisibility(View.GONE);

        }else {
            contador = contador + 1;
            num.push(contador);
        }
        Toast toast1 =
                Toast.makeText(getApplicationContext(),
                        "Ya no abran cupos disponibles en este horario", Toast.LENGTH_SHORT);

        toast1.show();
    }

    public void btnAction3(View view) {

        if (num.size() == 3){
            tres.setVisibility(View.GONE);

        }else {
            contador = contador + 1;
            num.push(contador);
        }
        Toast toast1 =
                Toast.makeText(getApplicationContext(),
                        "Ya no abran cupos disponibles en este horario", Toast.LENGTH_SHORT);

        toast1.show();
    }


    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar!=null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("Reserva YA!!");
        }
    }

    private void getDetailFood(final String foodId) {
        foods.child(foodId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                currentFood = dataSnapshot.getValue(Hotels.class);
                Picasso.get().load(((Hotels) currentFood).getImage()) .into (img_food);
                txtfood_price.setText(((Hotels) currentFood).getPrice());
                txtfood_name.setText(((Hotels) currentFood).getName());
                txtfood_description.setText(((Hotels) currentFood).getDescription());

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
