package com.example.miappmovil.ViewHolder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.miappmovil.Inferface.ItemClickListener;
import com.example.miappmovil.R;

public class ListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public TextView name_menu;
    public ImageView menu_image;
    public TextView food_menu;
    public ImageView food_image;

    private ItemClickListener itemClickListener;

    public ListViewHolder(@NonNull View itemView) {
        super(itemView);

        name_menu=(TextView)itemView.findViewById(R.id.food_menu);
        menu_image=(ImageView)itemView.findViewById(R.id.food_image);
        food_menu = itemView.findViewById(R.id.food_menu);
        food_image = itemView.findViewById(R.id.food_image);

        itemView.setOnClickListener(this);

    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View view) {
        itemClickListener.onClick(view,getAdapterPosition(),false);


    }
}
