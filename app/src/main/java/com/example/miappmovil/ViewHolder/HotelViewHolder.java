package com.example.miappmovil.ViewHolder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.miappmovil.Inferface.ItemClickListener;
import com.example.miappmovil.R;

public class HotelViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView txtFoodName;
    public ImageView imageFood;
    public TextView food_menu;
    public ImageView food_image;

    private ItemClickListener itemClickListener;

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public HotelViewHolder(@NonNull View itemView) {
        super(itemView);
        imageFood = itemView.findViewById(R.id.Imagen);
        txtFoodName = itemView.findViewById(R.id.Mendehabitaciones);
        food_menu = itemView.findViewById(R.id.food_menu);
        food_image = itemView.findViewById(R.id.food_image);

        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        itemClickListener.onClick(view,getAdapterPosition(),false);

    }
}
