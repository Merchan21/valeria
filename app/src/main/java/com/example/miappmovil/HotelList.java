package com.example.miappmovil;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import com.example.miappmovil.Common.Common;
import com.example.miappmovil.Inferface.ItemClickListener;
import com.example.miappmovil.Model.Hotels;
import com.example.miappmovil.ViewHolder.HotelViewHolder;
import com.example.miappmovil.ViewHolder.ListViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.mancj.materialsearchbar.MaterialSearchBar;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class HotelList extends AppCompatActivity {

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    FirebaseDatabase bd;
    DatabaseReference foodlist;
    String categoryId = "";
    FirebaseStorage storage;
    StorageReference storageReference;
    FirebaseRecyclerAdapter<Hotels, HotelViewHolder> adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotellist);


        bd=FirebaseDatabase.getInstance();
        foodlist=bd.getReference("Foods");
        storage= FirebaseStorage.getInstance();
        storageReference=storage.getReference();

        recyclerView = findViewById(R.id.recicler_food);
        recyclerView.setHasFixedSize(true);
        layoutManager= new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        if (getIntent()!=null)
            categoryId=getIntent().getStringExtra("CategoryId");
        if (!categoryId.isEmpty())
            loadListFood(categoryId);
    }


    private void loadListFood(String categoryId) {
        adapter=new FirebaseRecyclerAdapter<Hotels, HotelViewHolder>(
                Hotels.class,
                R.layout.hotelitem,
                HotelViewHolder.class,
                foodlist.orderByChild("menuId").equalTo(categoryId)

        ) {
            @Override
            protected void populateViewHolder(HotelViewHolder foodViewholder, Hotels foods, int i) {
                foodViewholder.txtFoodName.setText(foods.getName());
                Picasso.get().load(foods.getImage()).into(foodViewholder.imageFood);

                foodViewholder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {

                        Intent fooddetail=new Intent(HotelList.this,Hotel_Detail.class);
                        fooddetail.putExtra("FoodId",adapter.getRef(position).getKey());
                        startActivity(fooddetail);

                    }
                });

            }
        };
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Common.PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null) {
        }
    }

}
